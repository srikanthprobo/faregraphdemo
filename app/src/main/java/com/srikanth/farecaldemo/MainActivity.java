package com.srikanth.farecaldemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int minValue = 3000;
    private static final int maxValue = 4000;
    private static final int fareValue = 4000;
    private static final int CIRCLE_IMAGE_WIDTH = 40;

    private List<FareCalItem> fareCalItems = new ArrayList<>();
    private RecyclerView recyclerView;
    private FareCalenderSRPAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new FareCalenderSRPAdapter(fareCalItems, minValue, maxValue);
        recyclerView.setItemViewCacheSize(0);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        prepareFareCalData();

        /*int percentageOfIncreaseInFare = getPercentageOfIncreaseInFare(minValue, maxValue, fareValue);
        Log.i("MainActivity","percentageOfIncrease: "+percentageOfIncreaseInFare);
        
        setCircularImageOnLine(100-percentageOfIncreaseInFare);*/

    }

    private void prepareFareCalData() {
        fareCalItems.add(new FareCalItem(3300, "Mon", "11"));
        fareCalItems.add(new FareCalItem(3200, "Tue", "12"));
        fareCalItems.add(new FareCalItem(3500, "Wed", "13"));
        fareCalItems.add(new FareCalItem(3600, "Thu", "14"));
        fareCalItems.add(new FareCalItem(3400, "Fri", "15"));
        fareCalItems.add(new FareCalItem(3200, "Sat", "16"));
        fareCalItems.add(new FareCalItem(3700, "Sun", "17"));
        fareCalItems.add(new FareCalItem(3600, "Mon", "18"));
        fareCalItems.add(new FareCalItem(3300, "Tue", "19"));
        fareCalItems.add(new FareCalItem(3200, "Wed", "20"));

        mAdapter.notifyDataSetChanged();

    }

   /* private void setCircularImageOnLine(final int percentageOfIncreaseInFare) {
        final View view = findViewById(R.id.line_bg);
        ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    *//*Log.i("Line","Width: "+view.getWidth());
                    Log.i("Line","Height: "+view.getHeight());

                    Log.i("Line","Top: "+view.getTop());
                    Log.i("Line","Bottom: "+view.getBottom());
                    Log.i("Line","X: "+view.getX());
                    Log.i("Line","Y: "+view.getY());*//*

                    int yAxisValue = ((percentageOfIncreaseInFare * (view.getBottom() - view.getTop()) / 100) + view.getTop());
                    Log.i("MainActivity", "yAxisValue: " + yAxisValue);


                    RelativeLayout relativelayout = findViewById(R.id.parent_lyt);
                    LinearLayout.LayoutParams params = new LinearLayout
                            .LayoutParams(CIRCLE_IMAGE_WIDTH, CIRCLE_IMAGE_WIDTH);
                    // Add image path from drawable folder.
                    ImageView imageview = new ImageView(MainActivity.this);
                    imageview.setImageResource(R.drawable.circle_with_grey_border);
                    imageview.setLayoutParams(params);
                    imageview.setX(view.getX() - (CIRCLE_IMAGE_WIDTH / 2));
                    imageview.setY(yAxisValue - (CIRCLE_IMAGE_WIDTH / 2));
                    relativelayout.addView(imageview);
                }
            });
        }
    }*/

    private int getPercentageOfIncreaseInFare(int minValue, int maxValue, int fareValue) {
        int range = maxValue - minValue;
        int correctedStartValue = fareValue - minValue;
        return (correctedStartValue * 100) / range;
    }


}
