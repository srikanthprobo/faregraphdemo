package com.srikanth.farecaldemo;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by srikanth on 7/5/18.
 */

public class FareCalenderSRPAdapter extends RecyclerView.Adapter<FareCalenderSRPAdapter.MyViewHolder> {

    private List<FareCalItem> mFareCalItems;
    private int mMinRangeValue, mMaxRangeValue;
    private static final int CIRCLE_IMAGE_WIDTH = 40;
    private int mLineBottomValue, mLineTopValue;
    private float mLineXValue;


    public FareCalenderSRPAdapter(List<FareCalItem> mFareCalItems, int minRangeValue, int maxRangeValue) {
        this.mFareCalItems = mFareCalItems;
        mMinRangeValue = minRangeValue;
        mMaxRangeValue = maxRangeValue;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fare_cal_graph_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        FareCalItem fareCalItem = mFareCalItems.get(position);
        holder.day.setText(fareCalItem.getDay());
        holder.date.setText(fareCalItem.getDate());
        holder.fareCalValue.setText(""+fareCalItem.getFareValue());

        final int percentageOfIncreaseInFare =
                getPercentageOfIncreaseInFare(mMinRangeValue, mMaxRangeValue, fareCalItem.getFareValue());
        Log.i("FareCalenderSRPAdapter", "percentageOfIncrease: " + percentageOfIncreaseInFare);



        final View lineImg = holder.lineBg;
        int[] originalPos = new int[2];
        lineImg.getLocationInWindow(originalPos);

        lineImg.post(new Runnable() {
            @Override
            public void run() {

                if(lineImg.getBottom()!=0){
                    Log.i("FareCalenderSRPAdapter", "IFFF");
                    mLineBottomValue = lineImg.getBottom();
                    mLineTopValue = lineImg.getTop();
                    mLineXValue = lineImg.getX();
                    setCircularImageOnLine(100-percentageOfIncreaseInFare,holder.lineBg, holder.parentLyt);
                }else{
                    //setCircularImageOnLine(100-percentageOfIncreaseInFare,holder.lineBg, holder.parentLyt);
                    Log.i("FareCalenderSRPAdapter", "ELSEE");
                    setRateCircleOnLine(100-percentageOfIncreaseInFare,
                            mLineBottomValue, mLineTopValue, mLineXValue, holder.parentLyt);
                }

            }
        });

    }




    @Override
    public int getItemCount() {
        return mFareCalItems.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView day, date, fareCalValue;
        public View lineBg;
        public RelativeLayout parentLyt;

        public MyViewHolder(View view) {
            super(view);
            day = (TextView) view.findViewById(R.id.txt_fare_cal_day_value);
            date = (TextView) view.findViewById(R.id.txt_fare_cal_date_value);
            fareCalValue = (TextView) view.findViewById(R.id.txt_fare_cal_fare_value);
            lineBg = view.findViewById(R.id.view_fare_cal_line);
            parentLyt = (RelativeLayout) view.findViewById(R.id.lyt_circle);
        }
    }

    private int getPercentageOfIncreaseInFare(int minValue, int maxValue, int fareValue) {
        int range = maxValue - minValue;
        int correctedStartValue = fareValue - minValue;
        return (correctedStartValue * 100) / range;
    }


    private void setRateCircleOnLine(int percentageOfIncreaseInFare, int mLineBottomValue, int mLineTopValue, float mLineXValue, final RelativeLayout parentView) {
        int yAxisValue = ((percentageOfIncreaseInFare * (mLineBottomValue - mLineTopValue) / 100) + mLineTopValue);
        Log.i("FareCalenderSRPAdapter", "yAxisValue: " + yAxisValue);


        LinearLayout.LayoutParams params = new LinearLayout
                .LayoutParams(CIRCLE_IMAGE_WIDTH, CIRCLE_IMAGE_WIDTH);
        // Add image path from drawable folder.
        ImageView imageview = new ImageView(parentView.getContext());
        imageview.setImageResource(R.drawable.circle_with_grey_border);
        imageview.setLayoutParams(params);
        imageview.setX(mLineXValue - (CIRCLE_IMAGE_WIDTH / 2));
        imageview.setY(yAxisValue - (CIRCLE_IMAGE_WIDTH / 2));
        parentView.removeAllViews();
        parentView.addView(imageview);
    }

    private void setCircularImageOnLine(final int percentageOfIncreaseInFare, final View lineImage, final RelativeLayout parentView) {

        ViewTreeObserver viewTreeObserver = lineImage.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    lineImage.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    /*Log.i("Line","Width: "+view.getWidth());
                    Log.i("Line","Height: "+view.getHeight());

                    Log.i("Line","Top: "+view.getTop());
                    Log.i("Line","Bottom: "+view.getBottom());
                    Log.i("Line","X: "+view.getX());
                    Log.i("Line","Y: "+view.getY());*/

                    int yAxisValue = ((percentageOfIncreaseInFare * (lineImage.getBottom() - lineImage.getTop()) / 100) + lineImage.getTop());
                    Log.i("FareCalenderSRPAdapter", "yAxisValue: " + yAxisValue);


                    LinearLayout.LayoutParams params = new LinearLayout
                            .LayoutParams(CIRCLE_IMAGE_WIDTH, CIRCLE_IMAGE_WIDTH);
                    // Add image path from drawable folder.
                    ImageView imageview = new ImageView(lineImage.getContext());
                    imageview.setImageResource(R.drawable.circle_with_grey_border);
                    imageview.setLayoutParams(params);
                    imageview.setX(lineImage.getX() - (CIRCLE_IMAGE_WIDTH / 2));
                    imageview.setY(yAxisValue - (CIRCLE_IMAGE_WIDTH / 2));
                    parentView.removeAllViews();
                    parentView.addView(imageview);
                }
            });
        }else{
            Log.i("FareCalenderSRPAdapter", "ViewTreeNOTALIVE "+percentageOfIncreaseInFare);
        }
    }
}
