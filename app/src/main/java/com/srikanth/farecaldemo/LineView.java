package com.srikanth.farecaldemo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by srikanth on 8/5/18.
 */

public class LineView extends View {
    Paint paint = new Paint();
    PointF pointA, pointB;
    public LineView(Context context) {
        super(context);
        setWillNotDraw(false);
    }

    public LineView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setWillNotDraw(false);
    }

    public LineView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setWillNotDraw(false);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        paint.setColor(Color.RED);
        paint.setStrokeWidth(5);
        canvas.drawLine(pointA.x, pointA.y, pointB.x, pointB.y, paint);
        super.onDraw(canvas);
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        paint.setColor(Color.RED);
        paint.setStrokeWidth(5);
        canvas.drawLine(pointA.x, pointA.y, pointB.x, pointB.y, paint);
        super.dispatchDraw(canvas);

    }

    public void setPointA(PointF pointA) {
        this.pointA = pointA;
    }

    public void setPointB(PointF pointB) {
        this.pointB = pointB;
    }

    public void draw(){
        setWillNotDraw(false);
        invalidate();
        requestLayout();
    }
}
