package com.srikanth.farecaldemo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {
    private static final int minValue = 3000;
    private static final int maxValue = 4000;
    private static final int CIRCLE_IMAGE_WIDTH = 40;

    private List<FareCalItem> mFareCalItems = new ArrayList<>();
    private List<CoOrdinatePojo> mCircleCoordinates = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        prepareFareCalData();
        createViewsAndAddToScreen();

    }

    private void drawConnectingLines() {
        for(int i=0; i<mCircleCoordinates.size(); i++){
            CoOrdinatePojo currentItemCoOrdinate = mCircleCoordinates.get(i);

            if(i+1<mCircleCoordinates.size()){



                CoOrdinatePojo nextItemCoOrdinate = mCircleCoordinates.get(i+1);
                float centerY = getYCoOrdinateOFCenterPoint(currentItemCoOrdinate.getYCoOrdinate(), nextItemCoOrdinate.getYCoOrdinate());

                //TODO: From Layout findbyID intialize below Line fiew
                LinearLayout linearLayout = (LinearLayout) findViewById(R.id.lyt_parent_linear);
                LineView drawView = linearLayout.getChildAt(i).findViewById(R.id.lineDrawView);
                //LineView drawView = new LineView(this);
                drawView.setPointA(new PointF(currentItemCoOrdinate.getXCoOrdinate(), currentItemCoOrdinate.getYCoOrdinate()));
                drawView.setPointB(new PointF(2*currentItemCoOrdinate.getXCoOrdinate(), centerY));
                /*LineView drawView = new LineView(HomeActivity.this,
                        currentItemCoOrdinate.getXCoOrdinate(), currentItemCoOrdinate.getYCoOrdinate(),
                        2*currentItemCoOrdinate.getXCoOrdinate(), centerY);*/
                drawView.draw();



                /*   Bitmap bitmap = Bitmap.createBitmap((int) getWindowManager()
                        .getDefaultDisplay().getWidth(), (int) getWindowManager()
                        .getDefaultDisplay().getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                drawView.draw(canvas);*/


                /*Log.i("HomeActivity","X1: "+currentItemCoOrdinate.getXCoOrdinate());
                Log.i("HomeActivity","Y1: "+currentItemCoOrdinate.getYCoOrdinate());
                Log.i("HomeActivity","X2: "+2*currentItemCoOrdinate.getXCoOrdinate());
                Log.i("HomeActivity","Y2: "+centerY);*/
            }

        }

    }

    private float getYCoOrdinateOFCenterPoint(float currentY, float nextY) {
        float difference  = (nextY - currentY)/2;
        return currentY + difference;
    }

    private void createViewsAndAddToScreen() {


        for (FareCalItem fareCalItem : mFareCalItems) {
            LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = vi.inflate(R.layout.fare_cal_graph_item, null);
            ((TextView) view.findViewById(R.id.txt_fare_cal_day_value)).setText(fareCalItem.getDay());
            ((TextView) view.findViewById(R.id.txt_fare_cal_date_value)).setText(fareCalItem.getDate());
            ((TextView) view.findViewById(R.id.txt_fare_cal_fare_value)).setText(getString(R.string.rupee) + fareCalItem.getFareValue());

            final int percentageOfIncreaseInFare =
                    getPercentageOfIncreaseInFare(minValue, maxValue, fareCalItem.getFareValue());
            setCircularImageOnLine(100 - percentageOfIncreaseInFare, view);


            //Add view to parent
            ViewGroup insertPoint = (ViewGroup) findViewById(R.id.lyt_parent_linear);
            insertPoint.addView(view);
        }
    }


    private void setCircularImageOnLine(final int percentageOfIncreaseInFare, final View parentView) {
        final View view = parentView.findViewById(R.id.view_fare_cal_line);
        ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                   /*Log.i("Line","Width: "+view.getWidth());
                    Log.i("Line","Height: "+view.getHeight());

                    Log.i("Line","Top: "+view.getTop());
                    Log.i("Line","Bottom: "+view.getBottom());
                    Log.i("Line","X: "+view.getX());
                    Log.i("Line","Y: "+view.getY());*/

                    int yAxisValue = ((percentageOfIncreaseInFare * (view.getBottom() - view.getTop()) / 100) + view.getTop());
                    // Log.i("MainActivity", "yAxisValue: " + yAxisValue);


                    RelativeLayout relativelayout = parentView.findViewById(R.id.lyt_parent_fare_cal_item);
                    LinearLayout.LayoutParams params = new LinearLayout
                            .LayoutParams(CIRCLE_IMAGE_WIDTH, CIRCLE_IMAGE_WIDTH);
                    // Add image path from drawable folder.
                    ImageView imageview = new ImageView(HomeActivity.this);
                    imageview.setImageResource(R.drawable.circle_with_grey_border);
                    imageview.setLayoutParams(params);
                    imageview.setX(view.getX() - (CIRCLE_IMAGE_WIDTH / 2));
                    imageview.setY(yAxisValue - (CIRCLE_IMAGE_WIDTH / 2));

                    mCircleCoordinates.add(new CoOrdinatePojo(view.getX() - (CIRCLE_IMAGE_WIDTH / 2),
                            yAxisValue - (CIRCLE_IMAGE_WIDTH / 2)));
                    relativelayout.addView(imageview);

                    if(mCircleCoordinates.size()==mFareCalItems.size()){
                        drawConnectingLines();
                    }
                }
            });
        }
    }

    private void prepareFareCalData() {
        mFareCalItems.add(new FareCalItem(3300, "Mon", "11"));
        mFareCalItems.add(new FareCalItem(3200, "Tue", "12"));
        mFareCalItems.add(new FareCalItem(3500, "Wed", "13"));
        mFareCalItems.add(new FareCalItem(3600, "Thu", "14"));
        mFareCalItems.add(new FareCalItem(3400, "Fri", "15"));
        mFareCalItems.add(new FareCalItem(3200, "Sat", "16"));
        mFareCalItems.add(new FareCalItem(3700, "Sun", "17"));
        mFareCalItems.add(new FareCalItem(3600, "Mon", "18"));
        mFareCalItems.add(new FareCalItem(3300, "Tue", "19"));
        mFareCalItems.add(new FareCalItem(3200, "Wed", "20"));
    }

    private int getPercentageOfIncreaseInFare(int minValue, int maxValue, int fareValue) {
        int range = maxValue - minValue;
        int correctedStartValue = fareValue - minValue;
        return (correctedStartValue * 100) / range;
    }


    /*public class DrawView extends View {
        Paint paint = new Paint();
        View startView1;
        View endView1;
        float sX,sY,eX,eY;
        public DrawView(Context context, float sX, float sY, float eX, float eY) {
            super(context);
            //paint.reset();
            //paint.setStrokeWidth(3);
            setWillNotDraw(false);
            paint.setColor(Color.BLACK);
            paint.setStrokeWidth(1f);
            this.sX = sX;
            this.sY = sY;
            this.eX = eX;
            this.eY = eY;
        }



         @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawLine(sX, sY, eX, eY, paint);
        }
        *//*public void onDraw(Canvas canvas) {
            //paint.setColor(Color.GREEN);
            canvas.drawLine(sX, sY, eX, eY, paint);

        }*//*

    }*/

}
