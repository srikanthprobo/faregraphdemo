package com.srikanth.farecaldemo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

/**
 * Created by srikanth on 8/5/18.
 */

public class LineDrawView extends View {
    Paint paint = new Paint();
    float sX,sY,eX,eY;

    private void init() {
        setWillNotDraw(false);
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(3f);
    }

    public LineDrawView(Context context, float sX, float sY, float eX, float eY) {
        super(context);
        init();
        this.sX = sX;
        this.sY = sY;
        this.eX = eX;
        this.eY = eY;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawLine(sX, sY, eX, eY, paint);
        super.onDraw(canvas);

    }

    public void draw(){
        invalidate();
        requestLayout();
    }
}
